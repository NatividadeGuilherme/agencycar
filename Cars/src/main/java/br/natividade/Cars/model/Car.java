package br.natividade.Cars.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Range;

@Entity
public class Car {

	@Id
	private UUID id;
	
	public UUID getId() {
		return id;
	}
	
	@NotEmpty(message = "Modelo não pode estar vázio")
	private String modelo;
	
	@Range(min = 1900, max = 2900, message = "O ano tem que ser entre 1900 e 2900")
	private int ano;
	
	private boolean automatico;

	public Car() {
		super();
		this.id = UUID.randomUUID();
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public boolean isAutomatico() {
		return automatico;
	}

	public void setAutomatico(boolean automatico) {
		this.automatico = automatico;
	}

}
