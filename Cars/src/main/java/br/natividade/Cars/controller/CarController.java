package br.natividade.Cars.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.natividade.Cars.model.Car;
import br.natividade.Cars.repository.CarRepository;

@RestController
@RequestMapping("/car")
public class CarController {

	@Autowired
	private CarRepository repository;

	@PostMapping("/register")
	public ResponseEntity<Car> cadastrar(@Valid @RequestBody Car car) {
		Car carSaved = repository.saveAndFlush(car);

		return ResponseEntity.status(HttpStatus.CREATED).body(carSaved);
	}

	@GetMapping("/")
	public ResponseEntity<List<Car>> exibir() {
		return ResponseEntity.ok(repository.findAll());
	}
}
