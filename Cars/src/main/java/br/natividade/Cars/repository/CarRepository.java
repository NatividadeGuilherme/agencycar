package br.natividade.Cars.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.natividade.Cars.model.Car;

@Repository
public interface CarRepository extends JpaRepository<Car, UUID> {

}
